from clearml import Task
from clearml.automation.controller import PipelineController


# Connecting ClearML with the current process,
# from here on everything is logged automatically
task = Task.init(project_name='rabbit_fox', task_name='00001',
                 task_type=Task.TaskTypes.controller, reuse_last_task_id=False)

pipe = PipelineController(default_execution_queue='default', add_pipeline_tags=False)
pipe.add_step(name='annotation_conversion_train_copy', base_task_project='rabbit_fox', base_task_name='annotation_conversion_train')
pipe.add_step(name='annotation_conversion_test_copy', parents=['annotation_conversion_train_copy', ], base_task_project='rabbit_fox', base_task_name='annotation_conversion_test')
pipe.add_step(name='tf_convert_copy', parents=['annotation_conversion_test_copy', ], base_task_project='rabbit_fox', base_task_name='tf_convert')
pipe.add_step(name='train_nn_copy', parents=['tf_convert_copy', ], base_task_project='rabbit_fox', base_task_name='train_nn')

# Starting the pipeline (in the background)
pipe.start()
# Wait until pipeline terminates
pipe.wait()
# cleanup everything
pipe.stop()

print('done')