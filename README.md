# clml_od_toy

Toy project for ClearML Tensorflow 2.+ Object Detection

The project runs into 4 stages:
1) annotation_conversion_test.py runs process "annotation_conversion_test" which creates test.json file from json annotations in work/images/test folder. 
2) annotation_conversion_train.py runs process "annotation_conversion_train" which creates train.json file from json annotations in work/images/train folder. 
3) tf_create.py runs process "tf_convert", which creates test.tfrecord and train.tfrecord in work/tfrecord folder from files in work/images folder. 
4) main_model_tf2.py runs process "train_nn" which creates process "train_nn" for training artificial nn.  
